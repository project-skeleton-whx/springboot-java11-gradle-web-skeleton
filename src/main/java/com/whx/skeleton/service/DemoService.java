package com.whx.skeleton.service;

import org.springframework.stereotype.Service;

@Service
public class DemoService {

    public String hello(String name) {
        return String.format("Hello %s!", name);
    }
}
