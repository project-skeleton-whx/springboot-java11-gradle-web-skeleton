# SpringBoot Web 项目骨架

### 使用组件/依赖

* SpringBoot 2.4.4
* Java11
* Gradle 5.6.4
* Lombok

### 启动
```
./gradlew clean build

./gradlew bootRun
```

访问 curll 'http://localhost:8081/'