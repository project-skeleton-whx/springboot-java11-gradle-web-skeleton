package com.whx.skeleton.controller;

import com.whx.skeleton.service.DemoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class IndexController {

    private final DemoService demoService;

    @RequestMapping
    public String index(@RequestParam(required = false, defaultValue = "World") String name) {
        log.info("------------------ IndexController ------------------");
        return demoService.hello(name);
    }
}
